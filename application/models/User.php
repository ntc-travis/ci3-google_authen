<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

/*
CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`oauth_provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
`oauth_uid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
`first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
`last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
`full_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
`email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
`gender` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
`locale` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
`picture_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
`profile_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
`created` datetime NOT NULL,
`modified` datetime NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
 */
class User extends CI_Model {
	function __construct() {
		$this->tableName = 'users';
		$this->primaryKey = 'id';
	}
	public function checkUser($data = array()) {
		$this->db->select($this->primaryKey);
		$query = $this->db->from($this->tableName)
			->where(array(
				'oauth_provider' => $data['oauth_provider'],
				'oauth_uid' => $data['oauth_uid']))
			->get();
		$num_rows = $query->num_rows();
		if ($num_rows > 0) {
			$result = $query->row_array();
			$data['modified'] = date("Y-m-d H:i:s");
			$update = $this->db->update($this->tableName, $data, array('id' => $result['id']));
			$userID = $result['id'];
		} else {
			$data['created'] = date("Y-m-d H:i:s");
			$data['modified'] = date("Y-m-d H:i:s");
			$insert = $this->db->insert($this->tableName, $data);
			$userID = $this->db->insert_id();
		}
		return $userID ? $userID : FALSE;
	}
}