<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class GoogleAnalytics {
    var $ga_api_applicationName;
    var $ga_api_clientId;
    var $ga_api_clientSecret;
    var $ga_api_redirectUri;
    var $ga_api_developerKey;
    var $ga_api_profileId;
    var $client;
    var $access_token_ready;
    public function __construct() {
        $CI =&get_instance();
        $CI->config->load('google');
        require APPPATH .'third_party/google-login-api/apiClient.php';
        require APPPATH .'third_party/google-login-api/contrib/apiAnalyticsService.php';

        $this->ga_api_applicationName = $CI->config->item('application_name', 'googleanalytics');
        $this->ga_api_clientId        = $CI->config->item('client_id', 'googleanalytics');
        $this->ga_api_clientSecret    = $CI->config->item('client_secret', 'googleanalytics');
        $this->ga_api_redirectUri     = $CI->config->item('redirect_uri', 'googleanalytics');
        $this->ga_api_developerKey    = $CI->config->item('api_key', 'googleanalytics');
        $this->ga_api_profileId       = $CI->config->item('profile_id', 'googleanalytics');
        $this->ga_api_scopes          = $CI->config->item('scopes', 'googleanalytics');

        $this->client = new apiClient();
        $this->client->setApplicationName($this->ga_api_applicationName);
        $this->client->setClientId($this->ga_api_clientId);
        $this->client->setClientSecret($this->ga_api_clientSecret);
        $this->client->setRedirectUri($this->ga_api_redirectUri);
        $this->client->setDeveloperKey($this->ga_api_developerKey);
        $this->client->setScopes($this->ga_api_scopes);
        $this->client->setAccessType('online');
        $this->client->setApprovalPrompt('auto');
        $this->analytics = new apiAnalyticsService($this->client);

        //$this->access_token_ready = $this->client->getAccessToken();

        $this->profileId   = "ga:".$this->ga_api_profileId;
        $this->startDate = date('Y-m-d', strtotime('-31 days')); // 31 days from now
        $this->endDate   = date('Y-m-d'); // todays date
    }

    public function loginURL() {
        return $this->client->createAuthUrl();
    }
    public function getAuthenticate() {
        return $this->client->authenticate();
    }
    public function getAccessToken() {
        return $this->client->getAccessToken();
    }
    public function setAccessToken() {
        return $this->client->setAccessToken();
    }
    public function revokeToken() {
        return $this->client->revokeToken();
    }

    /**
     * 
     * @param type $type Type of result {users,newUsers,percentNewSessions,sessions,bounces}
     * https://developers.google.com/analytics/devguides/reporting/core/dimsmets
     * Use without ga:
     * @return int
     */
    public function getTotal($type = 'users') {
        //if ($this->access_token_ready) {
            
            try {
                $optParams = array('max-results' => '100');
                $results = $this->analytics->data_ga->get($this->profileId, $this->startDate, $this->endDate, 'ga:' . $type, $optParams);
                $ga_total = 0;
                 if(!empty($results['rows'])){
                    foreach ($results['rows'] as $row) {
                        foreach ($row as $cell) {
                            $ga_total = $cell;
                        }
                    }
                } else {
                    return 0;
                }
                return $ga_total;
            } catch (Exception $ex) {
                $error = $ex->getMessage();
                die($error);
            }
        //}
    }
    /**
     * 
     * @param type $dimension Type of dimension to filter 
     * {browser,browserVersion,operatingSystem,operatingSystemVersion,isMobile,isTablet,mobileDeviceBranding,mobileDeviceModel,mobileInputSelector,mobileDeviceInfo,mobileDeviceMarketingName,deviceCategory}
     * @param type $type Type of result 
     * {users,newUsers,percentNewSessions,sessions,bounces}
     * https://developers.google.com/analytics/devguides/reporting/core/dimsmets
     */
    public function getDimensions($dimension = 'browser', $type = 'users') {
        //if ($this->access_token_ready) {
           // $this->analytics = new apiAnalyticsService($this->client);
            try {
                $optParams = array(
                    'dimensions' => 'ga:' . $dimension,
                    'max-results' => '100');
                $results = $this->analytics->data_ga->get($this->profileId, $this->startDate, $this->endDate, 'ga:' . $type, $optParams);
                //dd($results);
                $ga_dimension = array();
               if(!empty($results['rows'])){
                    foreach ($results['rows'] as $row) {
                        $ga_dimension[$row[0]] = $row[1];
                    }
                } else {
                    return NULL;
                }
                return $ga_dimension;
            } catch (Exception $ex) {
                $error = $ex->getMessage();
                die($error);
            }
        //
    }

    public function getReportInfo($dimension,$metrics) {
       // if ($this->access_token_ready) {
           // $this->analytics = new apiAnalyticsService($this->client);
            $metrics   = "ga:".$metrics;
            $optParams = array(
                    'dimensions' => 'ga:' . $dimension,
                    'max-results' => '100');
            $results   = $this->analytics->data_ga->get($this->profileId, $this->startDate, $this->endDate, $metrics, $optParams);
            //$data = $results['rows']; //To send it to the view later
            $result_data = array();
            if(!empty($results['rows'])){
                foreach ($results['rows'] as $key => $value) {
                    $result_data[$value[0]] = $value[1];
                }
            }
            return $result_data;
        //}
        //return false;

    }
    public function getUserInfo() {
        //if ($this->access_token_ready) {
            //$this->analytics = new apiAnalyticsService($this->client);
            try {
                $metrics     = "ga:users";
                $optParams   = array('max-results' => '1');
                $results     = $this->analytics->data_ga->get($this->profileId, $this->startDate, $this->endDate, $metrics, $optParams);
                $result_data = array();
                if(!empty($results['profileInfo'])){
                    $result_data = array(
                        'account_id'               => $results['profileInfo']['accountId'],
                        'web_property_id'          => $results['profileInfo']['webPropertyId'],
                        'internal_web_property_id' => $results['profileInfo']['internalWebPropertyId'],
                        'Profile ID'               => $results['profileInfo']['profileId'],
                        'table_id'                 => $results['profileInfo']['tableId'],
                        'profile_name'             => $results['profileInfo']['profileName'],
                        );
                }

                 return $result_data;
            } catch (Exception $ex) {
                $error = $ex->getMessage();
                die($error);
            }
        //}
    }
}