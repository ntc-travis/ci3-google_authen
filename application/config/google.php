<?php
/*
*** Get scopes: https://developers.google.com/oauthplayground/
*** Server API key
	API key: AIzaSyAs39lEFSXABTpIGIDJg8f2dpwVBQGxQ-I
	Creation date: May 6, 2016, 12:22:23 AM
	Created by: meaning1984@gmail.com (you)

***OAuth 2.0 client IDs
	Client ID: 962562597208-5io591kbtrjgpp5st2ll8k26ug8dga33.apps.googleusercontent.com
	Client secret: 2eAHq4bgfsUav_TAEZGYqDLV
	Creation date: Mar 24, 2016, 4:01:04 PM

https://analytics.google.com/analytics/web/#report/defaultid/a25163461w48960217p49425343/

Account ID               = 25163461   ==>  a25163461
Web Property ID          = UA-25163461-1
Internal Web Property ID = 48960217   ==>  w48960217
Profile ID               = 49425343   ==>  p49425343
Table ID                 = ga:49425343
Profile Name             = www.namthanhcorp.com

*/
$config['googleplus']['application_name'] = 'Google Authen';
$config['googleplus']['client_id']        = '962562597208-5io591kbtrjgpp5st2ll8k26ug8dga33.apps.googleusercontent.com';
$config['googleplus']['client_secret']    = '2eAHq4bgfsUav_TAEZGYqDLV';
$config['googleplus']['redirect_uri']     = 'http://localhost:81/ci3.0.6/google/oauth2callback';
$config['googleplus']['api_key']          = 'AIzaSyAs39lEFSXABTpIGIDJg8f2dpwVBQGxQ';
$config['googleplus']['scopes']           = array(
					"https://www.googleapis.com/auth/plus.login",
					"https://www.googleapis.com/auth/plus.me",
                    "https://www.googleapis.com/auth/userinfo.email",
                    "https://www.googleapis.com/auth/userinfo.profile",
	);

$config['googleanalytics']['application_name'] = 'Google Analytics';
$config['googleanalytics']['client_id']        = '962562597208-5io591kbtrjgpp5st2ll8k26ug8dga33.apps.googleusercontent.com';
$config['googleanalytics']['client_secret']    = '2eAHq4bgfsUav_TAEZGYqDLV';
$config['googleanalytics']['redirect_uri']     = 'http://localhost:81/ci3.0.6/google/analytic';
$config['googleanalytics']['api_key']          = 'AIzaSyAs39lEFSXABTpIGIDJg8f2dpwVBQGxQ';
$config['googleanalytics']['profile_id']       = '49425343';
$config['googleanalytics']['scopes']           = array(
					"https://www.googleapis.com/auth/analytics",
					"https://www.googleapis.com/auth/analytics.edit",
					"https://www.googleapis.com/auth/analytics.manage.users",
					"https://www.googleapis.com/auth/analytics.manage.users.readonly",
					"https://www.googleapis.com/auth/analytics.provision",
                    "https://www.googleapis.com/auth/analytics.readonly",
		);

/* End of file google.php*/
/* Location: ./application/config/google.php */