<?php
class Analytic extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->library('googleanalytics');
		$this->session->userdata('google_authen','');
	}
	public function index() {
        if (isset($_GET['code'])) {
        	$getAuthen = $this->googleanalytics->getAuthenticate();
        	if($getAuthen){
        		if($this->googleanalytics->getAccessToken()){
					$this->session->set_userdata('google_authen',array(
						'login'        => true,
						'token'        => $this->googleanalytics->getAccessToken(),
						'google_analytics' => array(
							'profileInfo' => $this->googleanalytics->getUserInfo(),
							'browser'     => $this->googleanalytics->getDimensions('browser','sessions'),
							'sessions'    => array(
								'bucket_sessions'           => $this->googleanalytics->getDimensions('sessionDurationBucket','sessions')[0],
								'bucket_bounceRate'         => $this->googleanalytics->getDimensions('sessionDurationBucket','bounceRate')[0],
								'bucket_bounces'            => $this->googleanalytics->getDimensions('sessionDurationBucket','bounces')[0],
								'bucket_sessionDuration'    => $this->googleanalytics->getDimensions('sessionDurationBucket','sessionDuration')[0],
								'bucket_avgSessionDuration' => $this->googleanalytics->getDimensions('sessionDurationBucket','avgSessionDuration')[0],
								'bucket_hits'               => $this->googleanalytics->getDimensions('sessionDurationBucket','hits')[0],
								'report'               => $this->googleanalytics->getDimensions('source','organicSearches'),
								
								),
							'users'       => array(
								'user'                 => $this->googleanalytics->getTotal('users'),
								'sessions'             => $this->googleanalytics->getTotal('sessions'),
								'userType'             => $this->googleanalytics->getDimensions('userType','users')['New Visitor'],
								'sessionCount'         => $this->googleanalytics->getDimensions('sessionCount','users')[1],
								'daysSinceLastSession' => $this->googleanalytics->getDimensions('daysSinceLastSession','users')[0],
								'report'    => array(
										//'7dayUsers' => $this->googleanalytics->getDimensions('date','7dayUsers'),
										//'14dayUsers' => $this->googleanalytics->getDimensions('date','14dayUsers'),
										'30dayUsers' => $this->googleanalytics->getDimensions('date','30dayUsers')
									),
								),
							'operatingSystems'     => $this->googleanalytics->getDimensions('operatingSystem','sessions'),
							'report_sessions'      => $this->googleanalytics->getReportInfo('date','sessions'),
							'report_searchKeyword' => $this->googleanalytics->getReportInfo('searchKeyword','searchResultViews'),
							'report_eventAction'   => $this->googleanalytics->getReportInfo('eventAction','eventValue'),
				        )
				));
        		}
	        	redirect('google/analytic/char');
	     	}else{
				redirect('google/analytic');
	     	}
        }//end if
        $contents['login_url'] = $this->googleanalytics->loginURL();
		$this->load->view('googles/analytic/index',$contents);
        
	}

	public function char(){
		if($this->session->userdata['google_authen']['login'] != true){
			redirect('google/analytic');
		}
		$contents['google_analytics'] = $this->session->userdata['google_authen']['google_analytics'];
		$this->load->view('googles/analytic/char',$contents);
	}
	public function logout(){
		$this->session->sess_destroy();
		$this->googleanalytics->revokeToken();
		redirect('google/analytic');
	}
}