<?php
class Oauth2callback extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('googleplus');
        $this->load->model('user');
        $this->session->userdata('google_authen', '');
    }
    public function index()
    {
        //check session
        if (!empty($this->session->userdata['google_authen']['login']) && $this->session->userdata['google_authen']['login'] == true) {
            redirect('google/oauth2callback/profile');
        }

        if (isset($_GET['code'])) {
            $getAuthen = $this->googleplus->getAuthenticate();
            if ($getAuthen) {
                $userProfile = $this->googleplus->getUserInfo();
                $data        = array();
                if ($this->googleplus->getAccessToken()) {
                    //Preparing data for database insertion
                    $userData['oauth_provider'] = 'google';
                    $userData['oauth_uid']      = $userProfile['id'];
                    $userData['full_name']      = $userProfile['name'];
                    $userData['email']          = $userProfile['email'];
                    $userData['first_name']     = $userProfile['given_name'];
                    $userData['last_name']      = $userProfile['family_name'];
                    $userData['gender']         = $userProfile['gender'];
                    $userData['locale']         = $userProfile['locale'];
                    $userData['profile_url']    = $userProfile['link'];
                    $userData['picture_url']    = $userProfile['picture'];

                    //save or update user info
                    $userID = $this->user->checkUser($userData);
                    if (!empty($userID)) {
                        $data['user_profile'] = $userData;
                        $this->session->set_userdata('google_authen', array(
                            'login'        => true,
                            'token'        => $this->googleplus->getAccessToken(),
                            'user_profile' => $this->googleplus->getUserInfo(),
                        ));
                    } else {
                        $data['user_profile'] = array();
                    }
                }
                d(1);
                redirect('google/oauth2callback/profile');
            } else {
                d(2);
                redirect('google/oauth2callback');
            }
        }
        $contents['login_url'] = '';
        if ($this->uri->segment(2) == 'oauth2callback') {
            $contents['login_url'] = $this->googleplus->loginURL();
        }
        $this->load->view('googles/oauth/index', $contents);
    }
    public function profile()
    {
        if ($this->session->userdata['google_authen']['login'] != true) {
            redirect('google/oauth2callback');
        }
        $contents['user_profile'] = $this->session->userdata['google_authen']['user_profile'];
        $this->load->view('googles/oauth/profile', $contents);
    }
    public function logout()
    {
        $this->session->sess_destroy();
        $this->googleplus->revokeToken();
        redirect('google/oauth2callback');
    }
}
